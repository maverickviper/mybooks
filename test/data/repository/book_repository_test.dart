import 'package:books/data/data_source/book_remote_data_source.dart';
import 'package:books/data/network/endpoints/books_endpoint.dart';
import 'package:books/data/network/response/books_response.dart';
import 'package:books/data/repository/book_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/dummy_data.dart';
import 'book_repository_test.mocks.dart';

@GenerateMocks(<Type>[BookRemoteDataSource])
void main() {
  late BookRepository repository;

  late MockBookRemoteDataSource mockBookDataSource;

  setUp(() {
    mockBookDataSource = MockBookRemoteDataSource();
    repository = BookRepository(mockBookDataSource);
  });

  group('Book Remote Data Source', () {
    test('book success ', () async {
      BookResponse dummyResponse = DummyData.dummyResponse;

      // stubbing dummy response to the endpoint
      when(mockBookDataSource.fetchBooksInfo())
          .thenAnswer((Invocation realInvocation) async {
        return dummyResponse;
      });

      BookResponse response = await repository.getBookResponse();

      verify(repository.getBookResponse()).called(1);

      expect(response, dummyResponse);
    });
  });
}
