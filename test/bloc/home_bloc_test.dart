import 'package:books/data/repository/book_repository.dart';
import 'package:books/presentation/ui/books/bloc/books_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';

import '../helpers/dummy_data.dart';
import 'home_bloc_test.mocks.dart';

@GenerateMocks(<Type>[BookRepository])
void main() {
  late BooksBloc booksBloc;

  ///
  MockBookRepository mockBookRepository = MockBookRepository();

  setUp(() {
    booksBloc = BooksBloc(mockBookRepository);
  });

  group('Test - BooksBloc', () {
    test('Test - Initial state is BookState ', () {
      expect(booksBloc.state, const BooksLoadingState());
    });

    blocTest(
        'Test - Initial Event passed and expecting HomeLoadingState and HomeSuccessState ',
        build: () {
          //stubbing dummy response
          when(mockBookRepository.getBookResponse())
              .thenAnswer((Invocation realInvocation) async {
            return DummyData.dummyResponse;
          });
          return booksBloc;
        },
        act: (BooksBloc bloc) => bloc.add(const BookInitialEvent()),
        expect: () => <Object>[BooksSuccessState(DummyData.dummyResponse)]);

    blocTest('Test Error  ',
        build: () {
          //stubbing dummy response
          when(mockBookRepository.getBookResponse())
              .thenThrow(DummyData.unAuthenticatedException);
          return booksBloc;
        },
        act: (BooksBloc bloc) => bloc.add(const BookInitialEvent()),
        expect: () => <Object>[const BooksFailureState("There's an Error")]);
  });
}
