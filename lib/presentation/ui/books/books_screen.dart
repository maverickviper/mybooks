import 'package:auto_route/auto_route.dart';
import 'package:books/data/di/injector.dart';
import 'package:books/data/network/response/books_response.dart';
import 'package:books/data/repository/book_repository.dart';
import 'package:books/presentation/common/loader.dart';
import 'package:books/presentation/styles/text_variants.dart';
import 'package:books/presentation/ui/books/bloc/books_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

///
/// Books List Screen
///
@RoutePage()
class BooksScreen extends StatelessWidget {
  ///
  const BooksScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BooksBloc>(
      create: (BuildContext context) =>
          BooksBloc(injector<BookRepository>())..add(BookInitialEvent()),
      child: const BookInfoWidget(),
    );
  }
}

///
class BookInfoWidget extends StatelessWidget {
  ///
  const BookInfoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const TextVariant(
          'Books Info',
          variantType: TextVariantType.headlineMedium,
        ),
      ),
      body: const _BookListBody(),
    );
  }
}

class _BookListBody extends StatelessWidget {
  const _BookListBody();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BooksBloc, BooksState>(
        builder: (BuildContext context, BooksState state) {
      switch (state) {
        case BooksLoadingState():
          return const Loader();
        case BooksSuccessState():
          return BookListWidget(response: state.response);
        case BooksFailureState():
          return ErrorMessage(message: state.errorMessage);
        default:
          return const SizedBox.shrink();
      }
    });
  }
}

///
class ErrorMessage extends StatelessWidget {
  ///
  final String message;

  ///
  const ErrorMessage({super.key, required this.message});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: TextVariant(
        message,
        variantType: TextVariantType.bodyMedium,
        textAlign: TextAlign.center,
        color: Colors.red,
      ),
    );
  }
}

///
class BookListWidget extends StatelessWidget {
  ///
  final BookResponse response;

  ///
  const BookListWidget({super.key, required this.response});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int pos) {
        ReadingLogEntries entry = response.readingLogEntries![pos];
        return ListTile(
          title: TextVariant(
            entry.work?.title ?? '',
            variantType: TextVariantType.bodyLarge,
            fontWeight: FontWeight.w600,
          ),
          subtitle: TextVariant(entry.work?.authorNames?.firstOrNull ?? ''),
        );
      },
      itemCount: response.readingLogEntries?.length ?? 0,
    );
  }
}
