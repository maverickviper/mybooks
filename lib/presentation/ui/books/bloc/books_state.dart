part of 'books_bloc.dart';

///
@immutable
sealed class BooksState with EquatableMixin {
  const BooksState();
}

///
class BooksLoadingState extends BooksState {
  ///
  const BooksLoadingState();

  @override
  List<Object?> get props => <Object?>[];
}

///
class BooksSuccessState extends BooksState {
  ///
  final BookResponse response;

  ///
  const BooksSuccessState(this.response);

  @override
  List<Object?> get props => <Object?>[response];
}

///
class BooksFailureState extends BooksState {
  ///
  final String errorMessage;

  ///
  const BooksFailureState(this.errorMessage);

  @override
  List<Object?> get props => <Object?>[errorMessage];
}
