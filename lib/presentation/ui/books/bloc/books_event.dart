part of 'books_bloc.dart';

///
@immutable
sealed class BooksEvent {
  const BooksEvent();
}

///
class BookInitialEvent extends BooksEvent {
  ///
  const BookInitialEvent();
}
