import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:books/data/network/response/books_response.dart';
import 'package:books/data/repository/book_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'books_event.dart';

part 'books_state.dart';

///
/// [BooksBloc]
///
class BooksBloc extends Bloc<BooksEvent, BooksState> {

  ///
  BookRepository bookRespository;

  ///
  BooksBloc(this.bookRespository) : super(const BooksLoadingState()) {
    on<BookInitialEvent>(handleInitialEvent);
  }

  ///
  FutureOr<void> handleInitialEvent(
      BooksEvent event, Emitter<BooksState> emit) async {
    try {
      BookResponse response = await bookRespository.getBookResponse();
      emit(BooksSuccessState(response));
    } catch (e) {
      emit(const BooksFailureState("There's an Error"));
    }
  }
}
