// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:books/data/data_source/book_remote_data_source.dart' as _i5;
import 'package:books/data/network/dio/dio_client.dart' as _i3;
import 'package:books/data/network/endpoints/books_endpoint.dart' as _i4;
import 'package:books/data/repository/book_repository.dart' as _i6;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.singleton<_i3.DioClient>(() => _i3.DioClient());
    gh.factory<_i4.BooksEndpoint>(() => _i4.BooksEndpoint(gh<_i3.DioClient>()));
    gh.factory<_i5.BookRemoteDataSource>(
        () => _i5.BookRemoteDataSource(gh<_i4.BooksEndpoint>()));
    gh.factory<_i6.BookRepository>(
        () => _i6.BookRepository(gh<_i5.BookRemoteDataSource>()));
    return this;
  }
}
