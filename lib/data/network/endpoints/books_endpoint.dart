
import 'package:books/data/network/dio/dio_client.dart';
import 'package:books/data/network/response/books_response.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';

part 'books_endpoint.g.dart';

///
@RestApi()
@injectable
abstract class BooksEndpoint {

  ///
  @factoryMethod
  factory BooksEndpoint(DioClient dio) {
    return _BooksEndpoint(dio);
  }

  ///
  @GET('/people/mekBot/books/want-to-read.json')
  Future<BookResponse> getBookInfo();
}
