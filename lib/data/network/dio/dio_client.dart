
import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:injectable/injectable.dart';

///
/// [DioClient]
///
@singleton
class DioClient extends DioForNative {

  ///
  /// [DioClient]
  ///
  DioClient._() : super(BaseOptions(baseUrl: 'https://openlibrary.org'));

  ///
  factory DioClient() {
    return DioClient._();
  }
}
