// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'books_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookResponse _$BookResponseFromJson(Map<String, dynamic> json) => BookResponse(
      page: (json['page'] as num?)?.toInt(),
      numFound: (json['num_found'] as num?)?.toInt(),
      readingLogEntries: (json['reading_log_entries'] as List<dynamic>?)
          ?.map((e) => ReadingLogEntries.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BookResponseToJson(BookResponse instance) =>
    <String, dynamic>{
      'page': instance.page,
      'num_found': instance.numFound,
      'reading_log_entries':
          instance.readingLogEntries?.map((e) => e.toJson()).toList(),
    };

ReadingLogEntries _$ReadingLogEntriesFromJson(Map<String, dynamic> json) =>
    ReadingLogEntries(
      work: json['work'] == null
          ? null
          : Work.fromJson(json['work'] as Map<String, dynamic>),
      loggedEdition: json['logged_edition'] as String?,
      loggedDate: json['logged_date'] as String?,
    );

Map<String, dynamic> _$ReadingLogEntriesToJson(ReadingLogEntries instance) =>
    <String, dynamic>{
      'work': instance.work?.toJson(),
      'logged_edition': instance.loggedEdition,
      'logged_date': instance.loggedDate,
    };

Work _$WorkFromJson(Map<String, dynamic> json) => Work(
      title: json['title'] as String?,
      key: json['key'] as String?,
      authorKeys: (json['author_keys'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      authorNames: (json['author_names'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      firstPublishYear: (json['first_publish_year'] as num?)?.toInt(),
      editionKey: (json['edition_key'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      coverId: (json['cover_id'] as num?)?.toInt(),
      coverEditionKey: json['cover_edition_key'] as String?,
    );

Map<String, dynamic> _$WorkToJson(Work instance) => <String, dynamic>{
      'title': instance.title,
      'key': instance.key,
      'author_keys': instance.authorKeys,
      'author_names': instance.authorNames,
      'first_publish_year': instance.firstPublishYear,
      'edition_key': instance.editionKey,
      'cover_id': instance.coverId,
      'cover_edition_key': instance.coverEditionKey,
    };
