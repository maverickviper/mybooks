import 'package:json_annotation/json_annotation.dart';

part 'books_response.g.dart';

///
String url = 'https://openlibrary.org/people/mekBot/books/want-to-read.json';

///
@JsonSerializable(explicitToJson: true, fieldRename: FieldRename.snake)
class BookResponse {
  ///
  int? page;

  ///
  int? numFound;

  ///
  List<ReadingLogEntries>? readingLogEntries;

  ///
  BookResponse({this.page, this.numFound, this.readingLogEntries});

  ///
  factory BookResponse.fromJson(Map<String, dynamic> json) {
    return _$BookResponseFromJson(json);
  }

  ///
  Map<String, dynamic> toJson() {
    return _$BookResponseToJson(this);
  }
}

///
@JsonSerializable(explicitToJson: true, fieldRename: FieldRename.snake)
class ReadingLogEntries {
  ///
  Work? work;

  ///
  String? loggedEdition;

  ///
  String? loggedDate;

  ///
  ReadingLogEntries({this.work, this.loggedEdition, this.loggedDate});

  ///
  factory ReadingLogEntries.fromJson(Map<String, dynamic> json) {
    return _$ReadingLogEntriesFromJson(json);
  }

  ///
  Map<String, dynamic> toJson() {
    return _$ReadingLogEntriesToJson(this);
  }
}

///
@JsonSerializable(explicitToJson: true, fieldRename: FieldRename.snake)
class Work {
  ///
  String? title;

  ///
  String? key;

  ///
  List<String>? authorKeys;

  ///
  List<String>? authorNames;

  ///
  int? firstPublishYear;

  ///
  List<String>? editionKey;

  ///
  int? coverId;

  ///
  String? coverEditionKey;

  ///
  Work(
      {this.title,
      this.key,
      this.authorKeys,
      this.authorNames,
      this.firstPublishYear,
      this.editionKey,
      this.coverId,
      this.coverEditionKey});

  ///
  factory Work.fromJson(Map<String, dynamic> json) {
    return _$WorkFromJson(json);
  }

  ///
  Map<String, dynamic> toJson() {
    return _$WorkToJson(this);
  }
}
