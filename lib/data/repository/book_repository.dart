import 'package:books/data/data_source/book_remote_data_source.dart';
import 'package:books/data/network/response/books_response.dart';
import 'package:injectable/injectable.dart';

///
/// [BookRepository]
///
@injectable
class BookRepository {
  ///
  BookRemoteDataSource bookRemoteDataSource;

  ///
  BookRepository(this.bookRemoteDataSource);

  ///
  Future<BookResponse> getBookResponse() async {
    return bookRemoteDataSource.fetchBooksInfo();
  }
}
