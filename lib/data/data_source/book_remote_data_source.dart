import 'package:books/data/network/endpoints/books_endpoint.dart';
import 'package:books/data/network/response/books_response.dart';
import 'package:injectable/injectable.dart';

///
/// [BookRemoteDataSource]
///
@injectable
class BookRemoteDataSource {
  ///
  BooksEndpoint booksEndpoint;

  ///
  BookRemoteDataSource(this.booksEndpoint);

  ///
  Future<BookResponse> fetchBooksInfo() async {
    return booksEndpoint.getBookInfo();
  }
}
