import 'package:books/data/di/injector.dart';
import 'package:books/presentation/routing/router.dart';
import 'package:books/presentation/ui/books/books_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'presentation/styles/themes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt it = await configureDependencies();
  await it.allReady();
  runApp(MyApp());
}

///
class MyApp extends StatelessWidget {

  ///
  MyApp({super.key});

  ///
  final AppRouter router = AppRouter();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Books',
      routerConfig: router.config(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      theme: lightThemeData,
      darkTheme: darkThemeData,
    );
  }
}
